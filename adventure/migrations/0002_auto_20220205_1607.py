# Generated by Django 3.2.12 on 2022-02-05 16:07

from decimal import Decimal
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('adventure', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehicle',
            name='fuel_efficiency',
            field=models.DecimalField(decimal_places=2, default=Decimal('0.0'), max_digits=6),
        ),
        migrations.AddField(
            model_name='vehicle',
            name='fuel_tank_size',
            field=models.DecimalField(decimal_places=2, default=Decimal('0.0'), max_digits=6),
        ),
        migrations.CreateModel(
            name='ServiceArea',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kilometer', models.IntegerField()),
                ('gas_price', models.PositiveIntegerField()),
                ('left_station', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='left', to='adventure.servicearea')),
                ('right_station', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='right', to='adventure.servicearea')),
            ],
        ),
    ]
