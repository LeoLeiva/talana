import json
import pytest
from django.core import mail

from adventure import models, notifiers, repositories, usecases, views

from .test_02_usecases import MockJourneyRepository

#########
# Tests #
#########


class TestRepository:
    def test_create_vehicle(self, mocker):
        mocker.patch.object(models.Vehicle.objects, "create")
        repo = repositories.JourneyRepository()
        car = models.VehicleType()
        repo.create_vehicle(name="a", passengers=10, vehicle_type=car)
        assert models.Vehicle.objects.create.called


class TestNotifier:
    def test_send_notification(self, mocker):
        mocker.patch.object(mail, "send_mail")
        notifier = notifiers.Notifier()
        notifier.send_notifications(models.Journey())
        assert mail.send_mail.called


class TestCreateVehicleAPIView:
    def test_create(self, client, mocker):
        vehicle_type = models.VehicleType(name="car")
        mocker.patch.object(
            models.VehicleType.objects, "get", return_value=vehicle_type
        )
        mocker.patch.object(
            models.Vehicle.objects,
            "create",
            return_value=models.Vehicle(
                id=1, name="Kitt", passengers=4, vehicle_type=vehicle_type
            ),
        )
        payload = {"name": "Kitt", "passengers": 4, "vehicle_type": "car"}
        response = client.post("/api/adventure/create-vehicle/", payload)
        assert response.status_code == 201

class TestCreateServiceAreaAPIView:
    def test_create(self, client, mocker):
        mocker.patch.object(
            models.ServiceArea.objects,
            "create",
            return_value=models.ServiceArea(
                id=1, kilometer=60, gas_price=784
            ),
        )

        payload = {"kilometer":60, "gas_price":784}
        response = client.post("/api/adventure/create-service-area/", payload)
        assert response.status_code == 201

@pytest.mark.django_db
class TestGetVehicleAPIView:
    def test_get(self, client, mocker):
        vehicle = models.VehicleType.objects.create(
            name=str("Car"),
            max_capacity=4,
        )

        models.Vehicle.objects.create(
            name=str("VW"),
            passengers=4,
            vehicle_type=vehicle,
            number_plate=str("AB-12-89"),
        )

        response = client.get("/api/vehicles/")
        expected_data = {
            "id":1,
            "name":"VW",
            "passengers":4,
            "number_plate":"AB-12-89",
            "fuel_efficiency":"0.00",
            "fuel_tank_size":"0.00",
            "vehicle_type":1
        }

        assert response.status_code == 200
        assert response.json.func(response)[0] == expected_data

    def test_get_by_license_plate(self, client, mocker):
        vehicle = models.VehicleType.objects.create(
            name=str("Car"),
            max_capacity=4,
        )

        fiat = models.Vehicle.objects.create(
            name=str("Fiat"),
            passengers=4,
            vehicle_type=vehicle,
            number_plate=str("AB-12-89"),
        )

        renault = models.Vehicle.objects.create(
            name=str("Renault"),
            passengers=6,
            vehicle_type=vehicle,
            number_plate=str("AD-54-23"),
        )

        peugeot = models.Vehicle.objects.create(
            name=str("Peugeot"),
            passengers=2,
            vehicle_type=vehicle,
            number_plate=str("BC-23-54"),
        )

        response = client.get("/api/vehicles/?number_plate=BC-23-54")

        assert response.status_code == 200
        assert peugeot.number_plate in response.content.decode()
        assert renault.number_plate not in response.content.decode()
        assert fiat.number_plate not in response.content.decode()


@pytest.mark.django_db
class TestGetServiceAreaAPIView:
    def test_get(self, client, mocker):
        service_1 = models.ServiceArea.objects.create(
            kilometer=116,
            gas_price=91,
        )
        service_2 = models.ServiceArea.objects.create(
            kilometer=138,
            gas_price=95,
        )
        service_3 = models.ServiceArea.objects.create(
            kilometer=108,
            gas_price=95,
            right_station=service_2,
        )
        service_4 = models.ServiceArea.objects.create(
            kilometer=138,
            gas_price=95,
        )
        service_5 = models.ServiceArea.objects.create(
            kilometer=156,
            gas_price=90,
            left_station=service_1,
        )

        response = client.get("/api/service-area/")
        expected_data_1 = {
            'id': 1,
            'kilometer': 116,
            'gas_price': 91,
            'left_station': None,
            'right_station': None
        }
        expected_data_2 = {
            'id': 2,
            'kilometer': 138,
            'gas_price': 95,
            'left_station': None,
            'right_station': None
        }
        expected_data_3 = {
            'id': 3,
            'kilometer': 108,
            'gas_price': 95,
            'left_station': None,
            'right_station': 2
        }
        expected_data_4 = {
            'id': 4,
            'kilometer': 138,
            'gas_price': 95,
            'left_station': None,
            'right_station': None
        }
        expected_data_5 = {
            'id': 5,
            'kilometer': 156,
            'gas_price': 90,
            'left_station': 1,
            'right_station': None
        }

        assert response.status_code == 200
        assert response.json.func(response)[0] == expected_data_1
        assert response.json.func(response)[1] == expected_data_2
        assert response.json.func(response)[2] == expected_data_3
        assert response.json.func(response)[3] == expected_data_4
        assert response.json.func(response)[4] == expected_data_5

    def test_get_by_kilometer(self, client, mocker):
        service_1 = models.ServiceArea.objects.create(
            kilometer=116,
            gas_price=91,
        )
        service_2 = models.ServiceArea.objects.create(
            kilometer=138,
            gas_price=95,
        )
        service_3 = models.ServiceArea.objects.create(
            kilometer=108,
            gas_price=95,
            right_station=service_2,
        )
        service_4 = models.ServiceArea.objects.create(
            kilometer=138,
            gas_price=95,
        )
        service_5 = models.ServiceArea.objects.create(
            kilometer=156,
            gas_price=90,
            left_station=service_1,
        )

        response = client.get("/api/service-area/?kilometer=138")

        assert response.status_code == 200
        assert len(json.loads(response.content.decode())) == 2



class TestStartJourneyAPIView:
    def test_api(self, client, mocker):
        mocker.patch.object(
            views.StartJourneyAPIView,
            "get_repository",
            return_value=MockJourneyRepository(),
        )

        payload = {"name": "Kitt", "passengers": 2}
        response = client.post("/api/adventure/start/", payload)

        assert response.status_code == 201

    def test_api_fail(self, client, mocker):
        mocker.patch.object(
            views.StartJourneyAPIView,
            "get_repository",
            return_value=MockJourneyRepository(),
        )

        payload = {"name": "Kitt", "passengers": 6}
        response = client.post("/api/adventure/start/", payload)

        assert response.status_code == 400
